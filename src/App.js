/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Home from './pages/home';
import About from './pages/about';
import Resume from './pages/resume';
import Portfolio from './pages/portfolio';
import Contact from './pages/contact';

import Sidebar from './components/layout/sidebar';

const app = css`
  display: flex;
  height: 100%;
  width: 100%;
`;


function App() {
  return (
    <div css={app}>

      {/*<h1 css={css`
        color: peru;
      `}>hello world</h1> */}
      
      <Router>
        <Sidebar></Sidebar>
        <Switch>
          <Route path='/' exact><Home /></Route>
          <Route path='/about' exact><About /></Route>
          <Route path='/resume' exact><Resume /></Route>
          <Route path='/portfolio' exact><Portfolio /></Route>
          <Route path='/contact' exact><Contact /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
