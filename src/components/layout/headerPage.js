/** @jsx jsx */
import { css, jsx } from '@emotion/core';

function headerPage({ title }) {
  return (
    <div css={css`
      border-bottom: 1px dashed rgba(255, 255, 255, 0.2);
      margin: 2rem;

      h1 {
        color: #00A3E1;
        margin: 0px;
      }
    `}>
      <h1>{title}</h1>
    </div>
  )
}

export default headerPage


