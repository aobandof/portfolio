/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { Link } from 'react-router-dom';

import abel1 from '../../img/abel1.jpg';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faUser, faAddressCard, faBriefcase, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
// import { faUser as faUserRegular, faHome as faHomeRegular } from '@fortawesome/free-regular-svg-icons'


library.add(fab);


const app_link = css`
  color: turquoise;
  margin: .5rem 2rem;
  text-decoration: none;
`

function sidebar() {
  return (
    <div css={css`
    width: 200px;
    background-color: #17181B;
    display: flex;
    flex-direction: column;
    border-right: 1px solid #202226;
    color: #e1e1e1;
  `}>
      <div css={css`
        display: flex;
        flex-direction: column;
        height: auto;
        margin-bottom: 2rem;
        position: relative;
      `
      }>
        <h2 css={css`
          position: absolute;
          width: 100%;
          bottom: -1.2rem;
          text-align: center;
          background-color: #00a3e1b5;
          text-shadow: 0 2px 10px rgba(0, 0, 0, 0.5);
        `}>ABEL OBANDO</h2>
        <img css={css`
          width: 100%;
        `
        } src={abel1} alt=""/>
      </div>
      <div css={css`
        display: flex;
        flex-direction: column;
        flex: 1 0 auto;
      `}>
        <Link css={app_link} to='/'><FontAwesomeIcon icon={faHome} /> HOME</Link>
        <Link css={app_link} to='/about'><FontAwesomeIcon icon={faUser} /> ABOUT</Link>
        <Link css={app_link} to='/resume'><FontAwesomeIcon icon={faAddressCard} /> RESUME</Link>
        <Link css={app_link} to='/portfolio'><FontAwesomeIcon icon={faBriefcase} /> PORTFOLIO</Link>
        <Link css={app_link} to='/contact'><FontAwesomeIcon icon={faEnvelope} /> CONTACT</Link>
      </div>
      <div className="nav-footer">
        <ul className="social" css={css`
          display: flex;
          justify-content: center;
          width: 100%;
          list-style-type: none;
          padding: 0px;
          a {
            color: white;
            margin: .2rem .5rem;
            text-decoration: none;
          }
        `}>
          <li><a  target="_blanck" href="https://www.facebook.com/ofaber261182"><FontAwesomeIcon icon={['fab', 'facebook-square']} /></a></li>
          <li><a  target="_blanck" href="https://twitter.com/AbelObandof"><FontAwesomeIcon icon={['fab', 'twitter-square']} /></a></li>
          <li><a  target="_blanck" href="https://www.linkedin.com/in/aobandof/"><FontAwesomeIcon icon={['fab', 'linkedin']} /></a></li>
          <li><a  target="_blanck" href="https://github.com/aobandof"><FontAwesomeIcon icon={['fab', 'github']} /></a></li>
          <li><a  target="_blanck" href="https://gitlab.com/aobandof"><FontAwesomeIcon icon={['fab', 'gitlab']} /></a></li>
        </ul>
        <div css={css`
          width: 100%;
          p {
            text-align: center;
            font-size: .7rem;
          }
        `}>
          <p>2020 © aobandof.developer<br />All Right Reserved.</p>
        </div>
      </div>

    </div>

  )
}

export default sidebar


