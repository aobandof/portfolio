import React from 'react';

import HeaderPage from '../components/layout/headerPage';

const about = () => {
  return (
    <div className="page">
      <HeaderPage title='About Me' />
    </div>
  );
}

export default about;
