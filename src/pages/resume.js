import React from 'react';

import cv from '../assets/cv.pdf';

import HeaderPage from '../components/layout/headerPage';

const resume = () => {
  return (
    <div className="page">
      <HeaderPage title='Resume' />
      <div className="page-content">
        <p><a target="_blanck" href={cv}>Curriculum Vitae</a></p>
      </div>
    </div>
  );
}

export default resume;
