import React from 'react';

import HeaderPage from '../components/layout/headerPage';

const contact = () => {
  return (
    <div className="page">
      <HeaderPage title='Contact me' />
    </div>
  );
}

export default contact;
