import React from 'react';

import HeaderPage from '../components/layout/headerPage';

const home = () => {
  return (
    <div className="page">
      <HeaderPage title='Home' />
    </div>
  );
}

export default home;
